if ( $args[0] -eq "" ) { Write-Host "PID ikke inntastet, scriptet avsluttes..."; exit }         #Hvis ikke noen argumenter så avsluttes scriptet
else {
	for ($i = 0; $i -lt $args.Count; $i++) {
	   $id = $args[$i]
	   $dato = Get-Date -Format "yyyyMMdd-HHmmss"      #Finner dato
       $filnavn = "$id"+"-"+"$dato"+".meminfo"        # Lager filnavn
       Write-Host "Filnavn: $filnavn"

		Add-Content './PID-dato.meminfo' "******** Minneinfo om prosess med PID $($args[$i]) ********"        #Skriver Overskrift
		#Skriver totalt bruk av virtuelt minne til fil
		Add-Content './PID-dato.meminfo' "Total bruk av virtuelt minne: $((Get-Process -Id $args[$i]).VirtualMemorySize/ 1MB) MB" 
		#Skriver størrelse på working set til fil
		Add-Content './PID-dato.meminfo' "Størrelse på Working Set: $((Get-Process -Id $args[$i]).WorkingSet/ 1KB) KB`n`n"    
	}
}