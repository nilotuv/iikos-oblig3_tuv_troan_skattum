while($ans -ne 9){
	Write-Output "
	1 - Hvem er jeg og hva er navnet paa dette scriptet? 
	2 - Hvor lenge er det siden siste boot? 
	3 - Hvor mange prosesser og traader finnes? 
	4 - Hvor mange context switcher fant sted siste sekund? 
	5 - Hvor stor andel av CPU-tiden ble benyttet i kernelmode og i usermode siste sekund? 
	6 - Hvor mange interrupts fant sted siste sekund? 
	9 - Avslutt dette scriptet"

	$ans = Read-Host
	switch ($ans) {
		1 {
			$whoami = [Environment]::UserName
			$scriptnavn = $MyInvocation.MyCommand.Name
			Write-Output "Dette er $whoami og navnet paa scriptet er $scriptnavn"
		}
		2 {
			$uptime = (Get-CimInstance Win32_PerfFormattedData_PerfOS_System | findstr SystemUpTime)
			write-output "Tid siden siste boot: $uptime"
		}
		3 {
			$threads = ((Get-Process | Select-Object -ExpandProperty Threads).Count)
			$processes = ((Get-CimInstance Win32_OperatingSystem).NumberOfProcesses)
			write-output "Det er $processes prosesser og $threads traader"
		}
		4 {
			$switcher = ((Get-CimInstance Win32_PerfFormattedData_PerfOS_System).ContextSwitchesPersec)
			write-output "Switcher sist sekund $switcher"
		}
		5 {
			$kernel = ((Get-CimInstance Win32_PerfFormattedData_Counters_ProcessorInformation | Where-Object {$_.Name -eq "_Total"}).PercentUserTime)
			$user = ((Get-CimInstance Win32_PerfFormattedData_Counters_ProcessorInformation | Where-Object {$_.Name -eq "_Total"}).PercentPrivilegedTime)
			write-output "$kernel % brukt i kernelmode og $user % brukt i usermode"
		}
		6 {
			$interrupts=((Get-CimInstance Win32_PerfFormattedData_Counters_ProcessorInformation | Where-Object {$_.Name -eq "_Total"}).InterruptsPersec)
			write-output "Interrupts siste sekund: $interrupts"
		}
		9 {}
		default {
			Write-Output "Ugyldig verdi"
		}
	}
}