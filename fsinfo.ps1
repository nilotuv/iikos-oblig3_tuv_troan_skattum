
# hvor stor del av partisjonen som er full
$arg = Get-Item $args[0]                        
$partisjon = $arg.PSDrive                           
$partisjonBrukt = ($partisjon.Used / ($partisjon.used + $partisjon.free)) * 100  
write-output "Partisjonsbruk: $partisjonBrukt %"

# telle opp antall filer
$tellFiler = (Get-ChildItem -Path $arg -Recurse -Attributes !directory)
$antFiler = $tellFiler.count						         
write-output "antall filer: $antFiler"						         

# finne største fil
$storsteFil = ($tellFiler | sort Length -Descending)[0] 
$storsteFilNavn = $storsteFil.fullname
$storrelse = $storsteFil.Length
write-output "stoerste fil er $storsteFilNavn som er paa $storrelse b"

# gjennomsnitt
$total = $tellFiler | Measure-Object -Property Length -sum       
$gjennomsnitt = ($total.Sum / $total.Count)
write-output "gjennomsnittlig filstoerrelse: $gjennomsnitt b"
